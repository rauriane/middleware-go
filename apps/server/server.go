package main

import (
    "flag"
    "fmt"
    "net/http"

    "github.com/gorilla/mux"
    "github.com/rs/zerolog"
    "github.com/fsnotify/fsnotify"

    "cscs.ch/cicd-ext-mw/handler"
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


type LimitBodyMiddleware struct {
    limit int64
}
func (lmb *LimitBodyMiddleware) Middleware(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        r.Body = http.MaxBytesReader(w, r.Body, lmb.limit)
        next.ServeHTTP(w,r)
    })
}


func main() {
    logging.SetLogLevels(zerolog.WarnLevel, zerolog.DebugLevel, zerolog.InfoLevel)
    logger := logging.Get()

    var configpath string
    flag.StringVar(&configpath, "config", "config.yaml", "Path to config YAML file")

    flag.Parse()

    logger.Info().Msgf("Using configpath=%q", configpath)

    config := util.ReadConfig(configpath)

    util.SetDBPath(config.GetDBPath())
    db := util.GetDb()
    defer db.Close()

    util.SetupGitlab(config.Gitlab.Url, config.Gitlab.Token)
    glserver := util.GetGitlab()
    glserverJobToken := util.GetGitlabJobToken()

    util.SetTokenDeleterParams(glserver, db, config)

    // validate all templates working fine and install fsnotify watcher to listen to file changes
    util.SetTemplatesConfig(config.Templates)
    _ = util.TestAllTemplates()
    watcher, err := fsnotify.NewWatcher()
    if err != nil {
        // non-fatal, we can live without live changes of template files
        logger.Error().Err(err).Msg("Error creating new fsnotify watcher")
    } else {
        defer watcher.Close()
        util.ListenTemplateChanges(watcher)
        err = watcher.Add(config.Templates.Dir)
        if err != nil {
            logger.Error().Err(err).Msgf("Error adding directory %v to listen for file changes", config.Templates.Dir)
        }
    }

    // setup web routing rules
    reqHandler := mux.NewRouter()
    reqHandler.HandleFunc("/", handler.GetRootHandler()) // not really needed, this is a test handler

    pipeline_results_handler := handler.GetPipelineResultHandler(glserver, db, config)
    reqHandler.HandleFunc("/pipeline/results/{pipeline_id}/{gitlab_project_id}/{gitlab_pipeline_id}", pipeline_results_handler)
    reqHandler.HandleFunc("/pipeline/results/error/{uniq_error}", pipeline_results_handler)
    reqHandler.HandleFunc("/pipeline/restart", handler.GetPipelineRestartHandler(glserver, db))
    reqHandler.HandleFunc("/pipeline/cancel", handler.GetPipelineCanceltHandler(glserver, db))
    reqHandler.HandleFunc("/pipeline/trigger", handler.GetTriggerPipelineHandler(glserver, db, config))

    reqHandler.HandleFunc("/job/result/{pipeline_id}/{gitlab_project_id}/{job_id}", handler.GetJobResultsHandler(glserver, config))
    reqHandler.HandleFunc("/job/restart", handler.GetJobRestartHandler(glserver, db))
    reqHandler.HandleFunc("/job/cancel", handler.GetJobCancelHandler(glserver, db))
    reqHandler.HandleFunc("/job/play", handler.GetJobPlayHandler(glserver, db))

    reqHandler.HandleFunc("/image/clone", handler.GetCloneImageHandler(db, config))

    reqHandler.HandleFunc("/credentials", handler.GetCredentialsHandler(glserverJobToken, db, config))

    reqHandler.HandleFunc("/allowance/runner", handler.GetRunnerAllowanceHandler(glserverJobToken, db, config))

    reqHandler.HandleFunc("/k8s/liveness", handler.GetK8sLivenessHandler(db))

    reqHandler.HandleFunc("/setup/ui", handler.GetSetupCIHandler(glserver, db, config))

    reqHandler.HandleFunc("/webhook_gitlab_pipeline", handler.GetWebhookGitlabPipelineHandler(glserver, db, config))

    reqHandler.HandleFunc("/webhook_log", handler.GetWebhookLogHandler(config))

    reqHandler.HandleFunc("/webhook_ci", handler.GetWebhookCIHandler(glserver, db, config))

    reqHandler.HandleFunc("/container/build", handler.GetContainerBuildHandler(glserver, db, config))

// static requests are handled by apache directly
//    static_dir := "static"
//    reqHandler.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(static_dir)))).Methods("GET")

    // catch all other requests and simply return "404 - page not found"
    reqHandler.PathPrefix("/").Handler(handler.CatchAllHandler{})


    // install middlewares that
    // a) Restrict maximum body length to some reasonable size (1MB), we do NOT expect larger requests, thus it should be an error
    // b) Restrict every request, with all headers and full body. This way we can reproduce every request
    loggingMiddleware := logging.RequestLoggingMiddleware{Logger: logger}
    limitBodyMiddleware := LimitBodyMiddleware{config.MaxBodySize}
    reqHandler.Use(limitBodyMiddleware.Middleware)
    reqHandler.Use(loggingMiddleware.Middleware)

    listenAddress := fmt.Sprintf("%v:%v", config.Listen.Address, config.Listen.Port)
    logger.Info().Msgf("Starting server on %v", listenAddress)
    logger.Fatal().Err(http.ListenAndServe(listenAddress, reqHandler)).Msg("CI-Ext middleware closed")
}
