// Package ansihtml parses text formatted with ANSI escape sequences and
// outputs text suitable for display in a HTML <pre> tag.
//
// Text effects are encoded as <span> tags with style attributes or various
// classes.
package ansihtml

import (
    "io"
)

// ConvertToHTML converts ansiBytes to HTML where ANSI escape sequences have
// been translated to <span> tags with appropriate style attributes.
func ConvertToHTML(readFrom io.Reader, writeTo io.Writer) {
	convertToHTML(readFrom, writeTo, "", false, false)
}

// ConvertToHTMLWithClasses converts ansiBytes to HTML where ANSI escape
// sequences have been translated to <span> tags with appropriate classes set.
//
// classPrefix will be prefixed to the standard class names in the output.
//
// If noStyles is true, no style tags will be emitted for 256-color and 24-bit
// color text; instead, these color sequences will have no effect.
//
// A span in the output may have any combination of these classes:
//  * 'bold' or 'faint'
//  * 'italic' or 'fraktur'
//  * 'double-underline' or 'underline'
//  * 'strikethrough'
//  * 'overline'
//  * 'slow-blink' or 'fast-blink'
//  * 'invert'
//  * 'hide'
//  * one of 'font-{n}' where n is between 1 and 9
//  * 'proportional'
//  * 'superscript' or 'subscript'
//  * 'fg-{color}', 'bg-{color}', and 'underline-{color}' where color is one of
//   * black
//   * red
//   * green
//   * yellow
//   * blue
//   * magenta
//   * cyan
//   * white
//   * bright-black
//   * bright-red
//   * bright-green
//   * bright-yellow
//   * bright-blue
//   * bright-magenta
//   * bright-cyan
//   * bright-white
func ConvertToHTMLWithClasses(readFrom io.Reader, writeTo io.Writer, classPrefix string, noStyles bool) {
	convertToHTML(readFrom, writeTo, classPrefix, true, noStyles)
}

func convertToHTML(readFrom io.Reader, writeTo io.Writer, classPrefix string, useClasses, noStyles bool) {
	w := &htmlWriter{
		w:           writeTo,
		useClasses:  useClasses,
		noStyles:    noStyles,
		classPrefix: classPrefix,
	}
	p := NewParser(readFrom, w)
	err := p.Parse(w.handleEscape)
	w.closeSpan()
	// err must be nil since the underlying readers and writers
	// cannot return errors
	if err != nil {
		return
	}
}
