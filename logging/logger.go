package logging

import (
    "os"
    "sync"
    "time"

    "github.com/rs/zerolog"
    "gopkg.in/natefinch/lumberjack.v2"
)

var once sync.Once
var log zerolog.Logger
var consolelevel zerolog.Level
var filelevel zerolog.Level
var requestFileLevel zerolog.Level

type consoleLogHook struct {
    logger zerolog.Logger
}

func (h consoleLogHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
    if level >= h.logger.GetLevel() {
        h.logger.WithLevel(level).Msg(msg)
    }
}

func SetLogLevels(console, file, requestFile zerolog.Level) {
    consolelevel = console
    filelevel = file
    requestFileLevel = requestFile
}

func Get() *zerolog.Logger {
    once.Do(func() {
        zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

        consolelog := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}).
            Level(consolelevel).
            With().Timestamp().
            Logger()

        rotatedLogFile := lumberjack.Logger{
            Filename:   "log",
            MaxSize:    20, // megabytes
            MaxBackups: 20,
            MaxAge:     1000, //days
            LocalTime:  true, //use localtime timestamp when rotating
            Compress:   true, // disabled by default
        }
        log = zerolog.New(&rotatedLogFile).
            Level(filelevel).
            With().Timestamp().
            Caller().
            Logger().
            Hook(consoleLogHook{consolelog})
    })

    return &log
}
func Debug(msg string)                                 { log.Debug().Msg(msg) }
func Debugf(msgf string, v ...interface{})             { log.Debug().Msgf(msgf, v...) }
func Info(msg string)                                  { log.Info().Msg(msg) }
func Infof(msgf string, v ...interface{})              { log.Info().Msgf(msgf, v...) }
func Warn(msg string)                                  { log.Warn().Msg(msg) }
func Warnf(msgf string, v ...interface{})              { log.Warn().Msgf(msgf, v...) }
func Error(err error, msg string)                      { log.Error().Err(err).Msg(msg) }
func Errorf(err error, msgf string, v ...interface{})  { log.Error().Err(err).Msgf(msgf, v...) }
