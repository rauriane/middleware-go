package handler

import (
    "context"
    "fmt"
    "net/http"
    "strconv"
    "strings"

    "github.com/gorilla/mux"
    "github.com/xanzy/go-gitlab"
    "github.com/hasura/go-graphql-client"
    "golang.org/x/oauth2"
    "github.com/rs/zerolog"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


func GetPipelineResultHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(pipelineStatusHandler{glserver, db, config})
}

// static dict lookup tables and helper functions
func status_favicon(status string) string {
    switch strings.ToLower(status) {
        case util.GlStatusSuccess: return "check-circle-green.svg"
        case util.GlStatusFailed: return "x-circle-red.svg"
        case util.GlStatusRunning: return "hourglass-split-blue.svg"
        case util.GlStatusPending: return "pause-circle-yellow.svg"
        case util.GlStatusCanceled: return "slash-circle-black.svg"
        case util.GlStatusCreated: return "patch-question-black.svg"
        case util.GlStatusSkipped: return "patch-question-black.svg"
        default: return "patch-question-black.svg"
    }
}

func class_for_status(s string) string{
    switch strings.ToLower(s) {
        case util.GlStatusSuccess:  return "text-success"
        case util.GlStatusPending:  return "text-warning"
        case util.GlStatusFailed:   return "text-danger"
        case util.GlStatusRunning:  return "text-primary"
        case util.GlStatusCanceled: return  "text-dark"
        case util.GlStatusManual:  return "text-info"
        default: return "text-secondary"
    }
}

func combine_status(current, other string) string {
    current = strings.ToLower(current)
    other = strings.ToLower(other)
    if current == util.GlStatusCanceled || other == util.GlStatusCanceled { return util.GlStatusCanceled }
    if current == util.GlStatusFailed || other == util.GlStatusFailed { return util.GlStatusFailed }
    if current == util.GlStatusPending || other == util.GlStatusPending { return util.GlStatusPending }
    if current == util.GlStatusSuccess && other == util.GlStatusSuccess { return util.GlStatusSuccess }
    return other
}

func icon_for_status(status string) string {
    switch strings.ToLower(status) {
        case util.GlStatusSuccess: return "check-circle"
        case util.GlStatusPending: return "pause-circle"
        case util.GlStatusFailed: return "x-circle"
        case util.GlStatusRunning: return "hourglass-split"
        case util.GlStatusCanceled: return "slash-circle"
        case util.GlStatusManual: return "play-circle"
        default: return "patch-question"
    }
}

type pipelineStatusHandler struct {
    Glserver *gitlab.Client
    DB util.DB
    Config util.Config
}
func (h pipelineStatusHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    vars := mux.Vars(r)

    if uniq_error, key_found := vars["uniq_error"]; key_found {
        logger.Debug().Msgf("Requesting unique error page %v", uniq_error)
        http.ServeFile(w, r, fmt.Sprintf("%v/%v", h.Config.ErrorsDir, uniq_error))
        return
    }

    gitlab_project_id := vars["gitlab_project_id"]
    gitlab_pipeline_id, err := strconv.Atoi(vars["gitlab_pipeline_id"])
    pie(logger, err,"Invalid URL", http.StatusBadRequest)
    pipeline_id, err := strconv.ParseInt(vars["pipeline_id"], 10, 64)
    pie(logger, err, "Invalid URL", http.StatusBadRequest)

    m := h.DB.GetMirrorUrl(pipeline_id)
    if r.URL.Query().Get("type") == "gitlab" {
        redirect_url := fmt.Sprintf("%v/-/pipelines/%v", m, gitlab_pipeline_id)
        http.Redirect(w, r, redirect_url, http.StatusFound)
        return
    }


    repository_id := h.DB.GetRepositoryId(pipeline_id)
    repo_url := h.DB.GetRepoUrl(repository_id)
    repo_type := util.GetRepoType(repo_url)

    authorized_repo_id := authorized_for_repo(r)

    if r.URL.Query().Get("want_login") == "true" && authorized_repo_id != repository_id {
        w.Header().Set("WWW-Authenticate", `Basic realm="Access for job control", charset="UTF-8"`)
        http.Error(w, "Need login to project", http.StatusUnauthorized)
        return
    }

    gl_pipeline_iid, err := strconv.Atoi(r.URL.Query().Get("iid"))
    if err != nil {
        pipeline, resp, err := h.Glserver.Pipelines.GetPipeline(gitlab_project_id, gitlab_pipeline_id)
        pie(logger, err, "Error getting pipeline result", resp.StatusCode)
        gl_pipeline_iid = pipeline.IID
    }

    token := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: h.Config.Gitlab.Token})
    httpClient := oauth2.NewClient(context.Background(), token)
    client := graphql.NewClient(h.Config.Gitlab.Url + "/api/graphql", httpClient).WithDebug(true)
    variables := map[string]interface{} {
        "project_path": graphql.ID(fmt.Sprintf("%v/%v/%v", h.Config.Gitlab.MirrorsPath, repository_id, pipeline_id)),
        "pipeline_iid": graphql.ID(fmt.Sprintf("%v", gl_pipeline_iid)),
    }
    var query PipelineQuery
    err = client.Query(context.Background(), &query, variables)
    pie(logger, err, "Failed executing GraphQl query to fetch the pipeline status", http.StatusBadRequest)

    var pr_id string
    ref := query.Project.Pipeline.Ref
    if strings.HasPrefix(ref, "__CSCSCI__pr") {
        pr_id = strings.Replace(ref, "__CSCSCI__pr", "", 1)
        ref = fmt.Sprintf("PR %v", pr_id)
    }
    var pr_url string
    if len(pr_id) > 0 {
        switch repo_type {
        case util.Github:
            pr_url = fmt.Sprintf("%v/pull/%v", repo_url, pr_id)
        // case util.Github/util.Bitbucket TODO: gitlab/bitbucket
        }
    } else {
        switch repo_type {
        case util.Github:
            pr_url = fmt.Sprintf("%v/tree/%v", repo_url, ref)
        }
    }

    var commit_url string
    switch repo_type {
    case util.Github:
        if len(pr_id) > 0 {
            commit_url = fmt.Sprintf("%v/pull/%v/commits/%v", repo_url, pr_id, query.Project.Pipeline.Sha)
        } else {
            commit_url = fmt.Sprintf("%v/commit/%v", repo_url, query.Project.Pipeline.Sha)
        }
    // case util.Github/util.Bitbucket TODO: gitlab/bitbucket
    }
    tmpl_data := statusTmplData{
        Ref: ref,
        Title: fmt.Sprintf("%v - %v", h.DB.GetRepositoryName(repository_id), h.DB.GetPipelineName(pipeline_id)),
        PrUrl: pr_url,
        CommitUrl: commit_url,
        Favicon: status_favicon(query.Project.Pipeline.Status),
        UrlPrefix: h.Config.URLPrefix,
        ProjectID: int64(gl_gid_to_id(query.Project.Id)),
        PipelineID: pipeline_id,
        GitlabPipelineID: gl_gid_to_id(query.Project.Pipeline.Id),
        GitlabPipelineIid: gl_pipeline_iid,
        AuthorizedForRepo: authorized_repo_id==repository_id,
        CanCancel: query.Project.Pipeline.Cancelable,
        CanRestart: query.Project.Pipeline.Retryable,
        RestartCancelRedirect: fmt.Sprintf("/%v%v", h.Config.URLPrefix, r.URL),
        SHA: query.Project.Pipeline.Sha,
        Pipelines: parse_pipeline(&query.Project.Pipeline.PipelineCommon, query.Project.Pipeline.BridgeJobs.Nodes, "", h.Config.URLPrefix, pipeline_id,gl_gid_to_id(query.Project.Id), logger),
    }

    err = util.ExecuteTemplate(w, "pipeline_status.html", &tmpl_data)
    pie(logger, err, "Error rendering template pipeline_status.html", http.StatusInternalServerError)
}

func parse_pipeline(pipeline *PipelineCommon, bridge_jobs []Job, name string, urlprefix string, pipeline_id int64, gitlab_project_id int, logger *zerolog.Logger) []pipelineStatusTmplData {
    var ret []pipelineStatusTmplData
    var stages []stageTmplData
    ret = append(ret, pipelineStatusTmplData{
        Name: name,
        TextColor: class_for_status(pipeline.Status),
        Icon: icon_for_status(pipeline.Status),
    })
    for _, stage := range pipeline.Stages.Nodes {
        stage_has_jobs := false
        var groups []groupTmplData
        for _, group := range stage.Groups.Nodes {
            var jobs []jobTmplData
            for _, job := range group.Jobs.Nodes {
                if job.DownstreamPipeline == nil {
                    stage_has_jobs = true
                    var duration string
                    if job.Duration>0 {
                        duration = fmt.Sprintf("%v sec", int(job.Duration))
                    }
                    jobs = append(jobs, jobTmplData{
                        Status: job.Status,
                        Href: fmt.Sprintf("/%v/job/result/%v/%v/%v", urlprefix, pipeline_id, gitlab_project_id, gl_gid_to_id(job.Id)),
                        TextColor: class_for_status(job.Status),
                        Name: job.Name,
                        Icon: icon_for_status(job.Status),
                        Duration: duration,
                        JobID: gl_gid_to_id(job.Id),
                        CanRestart: job.Retryable,
                        CanCancel: job.Cancelable,
                    })
                } else {
                    var bridge_job *Job
                    for _, j := range bridge_jobs {
                        if j.DownstreamPipeline != nil && j.DownstreamPipeline.Iid == job.DownstreamPipeline.Iid {
                            bridge_job = &j
                            break
                        }
                    }
                    if bridge_job == nil {
                        pie(logger, fmt.Errorf("Could not find bridge job"), "Could not find bridge job", http.StatusInternalServerError)
                    }
                    ret = append(ret, parse_pipeline(bridge_job.DownstreamPipeline, bridge_jobs, job.Name, urlprefix, pipeline_id,gitlab_project_id, logger)...)
                }
            }
            groups = append(groups, groupTmplData{
                Name: group.Name,
                TextColor: class_for_status(group.DetailedStatus.Group), // this is where the standard status name is hidden
                Icon: icon_for_status(group.DetailedStatus.Group),
                Jobs: jobs,
            })
        }
        if stage_has_jobs {
            stages = append(stages, stageTmplData{
                Name: stage.Name,
                TextColor: class_for_status(stage.Status),
                Icon: icon_for_status(stage.Status),
                Groups: groups,
            })
        }
    }
    ret[0].Stages = stages
    return ret
}


// convert a gitlab GID to an integer. gid have this format: gid://gitlab/Ci::Build/4596751540
func gl_gid_to_id(gid string) int {
    ret, _ := strconv.Atoi(gid[strings.LastIndex(gid, "/")+1:])
    return ret
}

type statusTmplData struct {
    Ref string
    Title string
    PrUrl string
    CommitUrl string
    Favicon string
    UrlPrefix string
    ProjectID int64
    PipelineID int64 // database pipeline_id
    GitlabPipelineID int
    GitlabPipelineIid int
    AuthorizedForRepo bool
    CanRestart bool
    CanCancel bool
    RestartCancelRedirect string
    SHA string
    Pipelines []pipelineStatusTmplData
}
type pipelineStatusTmplData struct {
    Name string
    TextColor string
    Icon string
    Stages []stageTmplData
}

type stageTmplData struct {
    Name string
    TextColor string
    Icon string
    Groups []groupTmplData
}
type groupTmplData struct {
    Name string
    TextColor string
    Icon string
    Jobs []jobTmplData
}
type jobTmplData struct {
    Status string
    Href string
    TextColor string
    Name string
    Icon string
    Duration string
    JobID int
    CanRestart bool
    CanCancel bool
}

// GraphQL query
type PipelineQuery struct {
    Project Project `graphql:"project(fullPath: $project_path)"`
}
type Project struct {
    Name string
    Id string
    Pipeline PipelineWithJobs `graphql:"pipeline(iid: $pipeline_iid)"`
}
type PipelineCommon struct {
    Status string
    Id string
    Iid string
    Sha string `graphql:"sha(format: LONG)"`
    Ref string
    Cancelable bool
    Retryable bool
    Stages struct { Nodes []Stage }
}
type PipelineWithJobs struct {
    PipelineCommon
    BridgeJobs struct {
        Nodes []Job
    } `graphql:"jobs"`
}
type Stage struct {
    Status string
    Name string
    Groups struct { Nodes []Group }
}
type Group struct {
    Name string
    DetailedStatus struct {
        Group string
    }
    Jobs struct { Nodes []JobCommon }
}
type JobCommon struct {
    Name string
    Id string
    Status string
    Duration int
    Cancelable bool
    Retryable bool
    CanPlayJob bool
    AllowFailure bool
    FailureMessage string
    Stuck bool
    Tags []string
    WebPath string
    DownstreamPipeline *struct { Iid string}
}
type Job struct {
    JobCommon
    DownstreamPipeline *PipelineCommon
}
