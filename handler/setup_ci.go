package handler

import (
    "crypto/sha256"
    "encoding/base64"
    "encoding/json"
    "fmt"
    "net/http"
    "os"
    "regexp"
    "strconv"
    "strings"

    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetSetupCIHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(setupCIHandler{glserver, db, config})
}

type setupCIHandler struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}


func (h setupCIHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    var err error

    repo_id := authorized_for_repo(r)
    if repo_id == -1 || r.URL.Query().Get("ignore_login") == fmt.Sprint(repo_id) {
        w.Header().Set("WWW-Authenticate", `Basic realm="Access to CI setup page", charset="UTF-8"`)
        http.Error(w, "Need login to access setup page", http.StatusUnauthorized)
        return
    }

    // special case, where we want to test if a notification token is a valid token (requested from frontend)
    if repo_id != -1 && r.URL.Query().Get("test_notification_token") != "" {
        w.Write(h.testNotificationToken(repo_id, r.URL.Query().Get("test_notification_token")))
        return
    }

    selected_pipeline := r.URL.Query().Get("pipeline")
    pipeline_ids := h.db.GetPipelines(repo_id)
    var pipelines []pipelineTmplData
    for _, pipeline_id := range pipeline_ids {
        schedules := []pipelineScheduleTmplData{}
        schedule_data := h.db.GetPipelineSchedules(pipeline_id)
        for _, schedule := range schedule_data {
            cron_schedule := fmt.Sprintf("%v %v", schedule.CronSchedule, schedule.Ref)
            for key,value := range schedule.Variables {
                cron_schedule += fmt.Sprintf(";%v=%v", key, value)
            }
            schedules = append(schedules, pipelineScheduleTmplData{schedule.ScheduleID, cron_schedule})
        }
        if len(schedules) == 0 {
            schedules = []pipelineScheduleTmplData{{-1, ""}}
        }
        pipelines = append(pipelines, pipelineTmplData{
            ID: pipeline_id,
            CIEntrypoint: h.db.GetCIEntrypoint(pipeline_id),
            Name: h.db.GetPipelineName(pipeline_id),
            TrustedUser: strings.Join(h.db.GetGitUsersPipeline(pipeline_id), ","),
            CIBranches: strings.Join(h.db.GetBranchesPipeline(pipeline_id), ","),
            Schedule: schedules,
            TriggerPR: h.db.GetTriggerPR(pipeline_id),
        })
    }

    repo_url := h.db.GetRepoUrl(repo_id)
    repo_type := util.GetRepoType(repo_url)
    webhook_url := fmt.Sprintf("%v/%v/webhook_ci?id=%v", h.config.BaseURL, h.config.URLPrefix, repo_id)
    var repo_webhook_url, repo_webhook_setup, repo_provider string
    switch repo_type {
    case util.Github:
        repo_provider = "github"
        repo_webhook_url = fmt.Sprintf("%v/settings/hooks", repo_url)
        repo_webhook_setup = fmt.Sprintf("Payload URL: %v", webhook_url)
        repo_webhook_setup += "\nContent type: application/json"
        repo_webhook_setup += "\nSecret: The webhook secret that was sent to you by the CSCS-CI administrator"
        repo_webhook_setup += "\nWhich events would you like to trigger this webhook: Send me everything"
    case util.Bitbucket:
        repo_provider = "bitbucket"
        repo_webhook_url = fmt.Sprintf("%v/admin/webhooks", repo_url)
        repo_webhook_setup = "Title: Choose any name (e.g. CSCS-CI)"
        repo_webhook_setup += fmt.Sprintf("\nURL: %v&secret=REPLACE_WITH_WEBHOOK_SECRET", webhook_url)
        repo_webhook_setup += "\nTriggers: Tick all checkboxes (some features might not work correctly if you skip some triggers)"
    case util.Gitlab:
        repo_provider = "gitlab"
        repo_webhook_url = fmt.Sprintf("%v/-/hooks", repo_url)
        repo_webhook_setup = fmt.Sprintf("URL: %v", webhook_url)
        repo_webhook_setup += "\nSecret token: The webhook secret that was sent to you by the CSCS-CI administrator"
        repo_webhook_setup += "\nTrigger: Tick all checkboxes (some features might not work correctly if you skip some triggers)"
    }

    ssh_public_key, err := os.ReadFile(h.config.GetSSHKeyPath(repo_id, false, true))
    pie(logger, err, "Failed reading SSH public key", http.StatusInternalServerError)
    pkey, err := base64.StdEncoding.DecodeString(strings.Split(string(ssh_public_key), " ")[1])
    pie(logger, err, "Failed base64 decoding the public key", http.StatusInternalServerError)
    pkey_sha256 := sha256.Sum256(pkey)
    ssh_public_key_fingerprint := "SHA256:" + base64.RawStdEncoding.EncodeToString(pkey_sha256[:])

    tmpl_data := setupCITmplData{
        RepoName: h.db.GetRepositoryName(repo_id),
        RepoID: repo_id,
        RepoURL: repo_url,
        IsPrivate: h.db.IsPrivateRepo(repo_id),
        TrustedUser: strings.Join(h.db.GetGitUsersDefault(repo_id), ","),
        CIBranches: strings.Join(h.db.GetBranchesDefault(repo_id), ","),
        Pipelines: pipelines,
        SelectedPipeline: selected_pipeline,
        UrlPrefix: h.config.URLPrefix,
        RepoProvider: repo_provider,
        RepoWebhookUrl: repo_webhook_url,
        RepoWebhookSetup : repo_webhook_setup,
        SSHKeyPublic: string(ssh_public_key),
        SSHKeyPublicFingerprint: ssh_public_key_fingerprint,
        Favicon: "setup-favicon.svg",
    }

    err = util.ExecuteTemplate(w, "setup_ci.html", &tmpl_data)
    pie(logger, err, "Error rendering template setup_ci.html", http.StatusInternalServerError)
}

type setupCITmplData struct {
    RepoName string
    RepoID int64
    RepoURL string
    IsPrivate bool
    TrustedUser string
    CIBranches string
    Pipelines []pipelineTmplData
    SelectedPipeline string
    UrlPrefix string
    RepoProvider string
    RepoWebhookUrl string
    RepoWebhookSetup string
    SSHKeyPublic string
    SSHKeyPublicFingerprint string
    Favicon string
}

type pipelineTmplData struct {
    ID int64
    CIEntrypoint string
    Name string
    TrustedUser string
    CIBranches string
    Schedule []pipelineScheduleTmplData
    TriggerPR bool
}

type pipelineScheduleTmplData struct {
    ScheduleID int64
    CronSchedule string
}

func save_setup(r *http.Request, db util.DB, glserver *gitlab.Client, config util.Config) int64 {
    logger := logging.GetReqLogger(r)
    r.ParseForm()
    authorized_for_repo_id := authorized_for_repo(r)
    repo_id, err := strconv.ParseInt(r.FormValue("repository_id"), 10, 64)
    pie(logger, err, "Failed parsing repository_id as an integer", http.StatusBadRequest)

    // fail if a malicious request comes in, where the authorization header claims a different ID than the form data to which we will save claims
    if authorized_for_repo_id != repo_id {
        pie(logger, condition_error{"You are not authorized for this repository"}, "Invalid authorization", http.StatusForbidden)
    }

    repo_name := r.FormValue("name")
    private_repo := r.FormValue("privaterepo") != ""
    notification_token := r.FormValue("notification_token")
    trusted_user := r.FormValue("trusted_user")
    ci_branches := r.FormValue("ci_branches")


    if  notification_token != "" {
        db.UpdateNotificationToken(repo_id, notification_token)
    }

    // we split at comma, but also trim any whitespace
    my_split := func(in string) []string {
        splitted := strings.Split(in, ",")
        trimmed := util.SliceMap(splitted, func(x string)string{return strings.TrimSpace(x)})
        return util.SliceFilter(trimmed, func(x string) bool { return x!="" })
    }
    db.ReplaceGitUsersDefault(repo_id, my_split(trusted_user))
    db.ReplaceBranchesDefault(repo_id, my_split(ci_branches))

    name_changed := db.GetRepositoryName(repo_id) != repo_name
    private_changed := private_repo != db.IsPrivateRepo(repo_id)
    visibility := gitlab.PrivateVisibility
    if !private_repo {
        visibility = gitlab.PublicVisibility
    }

    // if new visibility is public we must first make sure that the group becomes public,
    // only then projects can be set to public visibility.
    // The opposite applies when we want to make it private, then we have to update
    // first all projects to visibility=private beore updating the group
    updateGroup := func() {
        if name_changed || private_changed {
            logger.Info().Msg("Name or Private flag changed. Updating Group on gitlab")
            db.UpdateRepositoryName(repo_id, repo_name)
            db.UpdatePrivateRepoFlag(repo_id, private_repo)
            newName := fmt.Sprintf("%v-%v", repo_name, repo_id)
            updates := &gitlab.UpdateGroupOptions{
                Name: &newName,
                Visibility: &visibility,
            }
            glserver.Groups.UpdateGroup(fmt.Sprintf("%v/%v", config.Gitlab.MirrorsPath, repo_id), updates)
        }
    }
    if visibility == gitlab.PublicVisibility {
        updateGroup()
    }

    pipeline_ids := db.GetPipelines(repo_id)
    for _, pipeline_id := range pipeline_ids {
        pipeline_name := r.FormValue(fmt.Sprintf("%v_name", pipeline_id))
        if matched, err := regexp.MatchString("^[a-zA-Z0-9\\-]+$", pipeline_name); err != nil || matched == false {
            this_error := condition_error{fmt.Sprintf("Invalid pipeline name %v", pipeline_name)}
            pie(logger, this_error, this_error.Error(), http.StatusBadRequest)
        }
        entrypoint := r.FormValue(fmt.Sprintf("%v_entrypoint", pipeline_id))
        trusted_user = r.FormValue(fmt.Sprintf("%v_trusted_user", pipeline_id))
        ci_branches = r.FormValue(fmt.Sprintf("%v_ci_branches", pipeline_id))
        trigger_pr := r.FormValue(fmt.Sprintf("%v_trigger_pr", pipeline_id)) != ""
        db.ReplaceGitUsersPipeline(pipeline_id, my_split(trusted_user))
        db.ReplaceBranchesPipeline(pipeline_id, my_split(ci_branches))
        db.UpdateTriggerPRFlag(pipeline_id, trigger_pr)
        for _, schedule := range db.GetPipelineSchedules(pipeline_id) {
            new_cron_schedule := r.FormValue(fmt.Sprintf("%v_schedule_%v", pipeline_id, schedule.ScheduleID))
            if new_cron_schedule == "" {
                db.DeletePipelineSchedule(schedule.ScheduleID)
            } else if new_cron_schedule != schedule.CronSchedule {
                if matched, err := regexp.MatchString("^(|[0-9]+ ([0-9,*\\/\\-]+ ){4}.+)$", new_cron_schedule); err != nil || matched == false {
                    this_error := condition_error{fmt.Sprintf("The cron schedule %v is invalid", new_cron_schedule)}
                    pie(logger, this_error, this_error.Error(), http.StatusBadRequest)
                }
                cron_time, ref, variables := parse_schedule_line(new_cron_schedule)
                db.UpdatePipelineSchedule(schedule.ScheduleID, cron_time, ref, variables)
            }
        }
        new_schedule_idx := -1
        for {
            new_cron_schedule := r.FormValue(fmt.Sprintf("%v_schedule_%v", pipeline_id, new_schedule_idx))
            if new_cron_schedule == "" {
                break
            } else {
                if matched, err := regexp.MatchString("^(|[0-9]+ ([0-9,*\\/\\-]+ ){4}.+)$", new_cron_schedule); err != nil || matched == false {
                    this_error := condition_error{fmt.Sprintf("The cron schedule %v is invalid", new_cron_schedule)}
                    pie(logger, this_error, this_error.Error(), http.StatusBadRequest)
                }
                cron_time, ref, variables := parse_schedule_line(new_cron_schedule)
                db.AddPipelineSchedule(pipeline_id, cron_time, ref, variables)
                new_schedule_idx -= 1
            }
        }
        pipeline_name_changed := pipeline_name != db.GetPipelineName(pipeline_id)
        entrypoint_changed := entrypoint != db.GetCIEntrypoint(pipeline_id)
        if pipeline_name_changed || name_changed || entrypoint_changed || private_changed {
            db.UpdatePipelineName(pipeline_id, pipeline_name)
            db.UpdateCIEntryppoint(pipeline_id, entrypoint)
            newProjectName := fmt.Sprintf("%v-%v", repo_name, pipeline_name)
            edits := &gitlab.EditProjectOptions{
                Name: &newProjectName,
                CIConfigPath: &entrypoint,
                Visibility: &visibility,
            }
            // it can happen that the gitlab API call fails. E.g. a new pipeline is added and renamed. Normally the mirror is not created on the gitlab side at this point
            // because it would only be created the first time we see a need to actually create it. Hence we log it only as a warning, but normally this is fine and (sometimes) expected
            _, glresp, err := glserver.Projects.EditProject(fmt.Sprintf("%v/%v/%v", config.Gitlab.MirrorsPath, repo_id, pipeline_id), edits)
            if err != nil {
                logger.Warn().Msgf("error when trying to update project details. err=%v, glresp.StatusCode=%v. This can happen when the mirror has not yet been created.", err, glresp.StatusCode)
            }
            //pie(logger, err, "Failed editing pipeline mirror", glresp.StatusCode)
        }
    }

    if visibility == gitlab.PrivateVisibility {
        updateGroup()
    }

    return repo_id
}


func (h setupCIHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    repo_id := authorized_for_repo(r)
    if repo_id == -1 {
        w.Header().Set("WWW-Authenticate", `Basic realm="Access to CI setup page", charset="UTF-8"`)
        http.Error(w, "Need login to access setup page", http.StatusUnauthorized)
        return
    }

    save_setup(r, h.db, h.glserver, h.config)

    if r.URL.Query().Get("action") == "create" {
        new_pipeline_id, err := h.db.AddPipeline(repo_id, "unnamed")
        pie(logger, err, "Error adding new pipeline", http.StatusInternalServerError)
        http.Redirect(w, r, fmt.Sprintf("%v?pipeline=%v", r.FormValue("redirect_url"), new_pipeline_id), http.StatusFound)
    } else {
        if r.URL.Query().Get("action") == "delete" {
            pipeline_id, err := strconv.ParseInt(r.URL.Query().Get("pipeline_id"), 10, 64)
            pie(logger, err, "Failed parsing pipeline_id as integer", http.StatusBadRequest)
            // it can be that the project was never created, i.e. there is nothing to delete
            h.glserver.Projects.DeleteProject(fmt.Sprintf("%v/%v/%v", h.config.Gitlab.MirrorsPath, repo_id, pipeline_id))
            ssh_keys := [2]string{h.config.GetSSHKeyPath(pipeline_id, true, false), h.config.GetSSHKeyPath(pipeline_id, true, true)}
            for _, key_path := range ssh_keys {
                if err := os.Remove(key_path); err != nil {
                    logger.Error().Err(err).Msgf("Failed removing file %v", key_path)
                }
            }
            h.db.DeletePipeline(pipeline_id)
        }
        http.Redirect(w, r, r.FormValue("redirect_url"), http.StatusFound)
    }
}

func (h *setupCIHandler) testNotificationToken(repo_id int64, notification_token string) []byte {
    if notification_token == "__dbvalue" {
        notification_token = h.db.GetNotificationToken(repo_id)
        if notification_token == "" {
            return []byte("Notification token is not set. Please set a valid notification token")
        }
    }
    repo_url := h.db.GetRepoUrl(repo_id)
    notify_url := util.GetNotificationUrl(repo_url, "0000000000000000000000000000000000000000")
    resp, err := util.NotifyRepo(notify_url, util.GlStatusSuccess, fmt.Sprintf("%v/%v/setup/ui", h.config.BaseURL, h.config.URLPrefix), "notify_test", notification_token, "Test notification token")
    if err != nil {
        logging.Errorf(err, "Error while trying to test notification token for repo_id=%v", repo_id)
        return []byte(err.Error())
    }
    responseData := make(map[string]any)
    if err := json.Unmarshal(resp.ResponseData, &responseData); err != nil {
        logging.Errorf(err, "Failed unmarshalling data from JSON. responseData=%v", string(resp.ResponseData))
        return []byte(err.Error())
    }
    if util.GetRepoType(repo_url) == util.Github {
        if resp.StatusCode != 422 || responseData["message"] != "No commit found for SHA: 0000000000000000000000000000000000000000" {
            return []byte("Notification token is invalid")
        }

    } else if util.GetRepoType(repo_url) == util.Bitbucket {
        if resp.StatusCode != 200 || responseData["key"] != "cscs/notify_test" {
            return []byte("Notification token is invalid")
        }
    } else if util.GetRepoType(repo_url) == util.Gitlab {
        if resp.StatusCode != 404 || responseData["message"] != "404 Commit Not Found" {
            return []byte("Notification token is invalid")
        }
    }
    return []byte("ok")
}


func parse_schedule_line(cron_schedule_line string) (string, string, map[string]string) {
    split := strings.Split(cron_schedule_line, " ")
    cron_time := strings.Join(split[0:5], " ")
    ref_variables := strings.Split(strings.TrimSpace(split[5]), ";")
    ref := ref_variables[0]
    variables := make(map[string]string)
    for _, variable := range ref_variables[1:] {
        var_split := strings.SplitN(variable, "=", 2)
        variables[var_split[0]] = var_split[1]
    }
    return cron_time, ref, variables
}
