package handler

import (
    "encoding/json"
    "fmt"
    "net/http"
    "regexp"
    "strconv"
    "strings"

    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


func GetRunnerAllowanceHandler(glserverJobToken *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(runnerAllowanceHandler{Glserver: glserverJobToken, DB: db, Config: config})
}

type runnerAllowanceHandler struct {
    Glserver *gitlab.Client
    DB util.DB
    Config util.Config
}

type runnerAllowanceResp struct {
    Allowed string  `json:"allowed"`
    Mode string     `json:"mode"`
}

func (h runnerAllowanceHandler) Get(w http.ResponseWriter, r* http.Request) {
    logger := logging.GetReqLogger(r)
    job_token := r.URL.Query().Get("token")
    jobTokenOptions := gitlab.GetJobTokensJobOptions{JobToken: gitlab.String(job_token)}
    job, _, err := h.Glserver.Jobs.GetJobTokensJob(&jobTokenOptions)
    pie(logger, err, "Could not fetch token's job", http.StatusBadRequest)

    logger.Debug().Msgf("Job token=%v belongs to job_id=%v at %v", job_token, job.ID, job.WebURL)

    reg_exp := fmt.Sprintf("^%s/%s/([0-9]+)/([0-9]+)/.*/%d$", h.Config.Gitlab.Url, h.Config.Gitlab.MirrorsPath, job.ID)

    re := regexp.MustCompile(reg_exp)
    matches := re.FindStringSubmatch(job.WebURL)

    if len(matches) != 3 {
        pie(logger, condition_error{Err: "Job's web_url did not match the expected format"},
            fmt.Sprintf("web_url=%s did not match the expected format=%s", job.WebURL, reg_exp), http.StatusBadRequest)
    }

    if len(job.TagList) != 1 {
        pie(logger, condition_error{Err: "Job did not specify exactly one tag"},
            fmt.Sprintf("Specified tags=%s, count=%d", job.TagList, len(job.TagList)), http.StatusBadRequest)
    }
    _, mode, could_cut := strings.Cut(job.TagList[0], "-")
    if could_cut == false {
        pie(logger, condition_error{Err: "Could not split the job tag"},
            fmt.Sprintf("Specified tag=%s. Expected format: CLUSTER-RUNNERMODE", job.TagList[0]), http.StatusBadRequest)
    }

    repository_id, err := strconv.ParseInt(matches[1], 10, 64)
    pie(logger, err, "Job's web url did not match the expected format", http.StatusBadRequest)
    pipeline_id, err := strconv.ParseInt(matches[2], 10, 64)
    pie(logger, err, "Job's web url did not match the expected format", http.StatusBadRequest)

    if repository_id != h.DB.GetRepositoryId(pipeline_id) {
        pie(logger, condition_error{Err: "repository id in database and web url do not match"}, "Job's web_url did not match the expected format", http.StatusBadRequest)
    }

    tag_allowed,max_num_nodes := h.DB.GetRunnerAllowance(repository_id, job.TagList[0])
    allowed := fmt.Sprintf("You are not allowed to run on %v", job.TagList[0])
    if tag_allowed {
        allowed = "YES"

        // check if requested number of nodes is allowed
        num_nodes := 1
        if r.URL.Query().Has("num_nodes") {
            var err error
            num_nodes, err = strconv.Atoi(r.URL.Query().Get("num_nodes"))
            pie(logger, err, "Could not convert num_nodes to an integer", http.StatusBadRequest)
        }
        if num_nodes > max_num_nodes {
            allowed = fmt.Sprintf("You are not allowed to run on %v nodes. Allowed maximum=%v", num_nodes, max_num_nodes)
        }
    }

    w.Header().Add("Content-Type", "application/json")
    ret := runnerAllowanceResp{Allowed:allowed, Mode:mode}
    json.NewEncoder(w).Encode(ret)
}
