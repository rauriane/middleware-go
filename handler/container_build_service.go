package handler

import (
    "fmt"
    "io/ioutil"
    "net/http"
    "time"

    "github.com/xanzy/go-gitlab"
    "github.com/go-git/go-git/v5"
    gitconfig "github.com/go-git/go-git/v5/config"
    "github.com/go-git/go-git/v5/plumbing/object"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetContainerBuildHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(containerBuild{glserver, db, config})
}

type containerBuild struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

// body must be the Dockerfile to build - will redirect to a website, which has the results
func (h containerBuild) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    pipeline_id := h.config.Gitlab.ContainerBuilderServicePipelineId
    repo_id := h.db.GetRepositoryId(pipeline_id)
    new_branch := fmt.Sprintf("%v-%v", logging.GetReqCorrelationID(r), pipeline_id)
    git_target_path := fmt.Sprintf("home/tmp/container_build/%v", new_branch)

    util.LockRepoDir(git_target_path, logger)
    defer util.UnlockRepoDir(git_target_path, logger)

    repo, err := util.GitCloneOrOpen(&h.db, &h.config, git_target_path, repo_id, false, logger, nil)
    pie(logger, err, "Failed clonging/opening the container build project", http.StatusInternalServerError)

    err = util.GitFetch(&h.db, &h.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/main:refs/heads/main")}, repo_id, logger, nil)
    pie(logger, err, "Failed fetching main branch for container build project", http.StatusInternalServerError)

    err = util.EnsureMirrorRepo(&h.db, &h.config, h.glserver, logger, repo_id, pipeline_id)
    pie(logger, err, "Failed creating mirror repository", http.StatusInternalServerError)

    dockerfile_content, err := ioutil.ReadAll(r.Body)
    pie(logger, err, "Failed getting dockerfile from POST event", http.StatusBadRequest)
    err = ioutil.WriteFile(fmt.Sprintf("%v/ci/Dockerfile", git_target_path), dockerfile_content, 0600)
    pie(logger, err, "Failed writing Dockerfile", http.StatusInternalServerError)

    worktree, err := repo.Worktree()
    pie(logger, err, "Failed opening worktree from repo", http.StatusInternalServerError)
    _, err = worktree.Add("ci/Dockerfile")
    pie(logger, err, "Failed adding new Dockerfile to repository worktree", http.StatusInternalServerError)
    _, err = worktree.Commit("changed Dockerfile", &git.CommitOptions{
        Author: &object.Signature{Name: "CSCS CI-Ext", Email: "ciext@cscs.ch", When: time.Now()},
    })
    pie(logger, err, "failed commiting to worktree", http.StatusInternalServerError)

    refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/main:refs/heads/%v", new_branch))}
    err = util.GitPush(&h.db, &h.config, repo, refspecs, repo_id, pipeline_id, logger, nil)
    pie(logger, err, "Failed pushing changes to mirror repository", http.StatusInternalServerError)

    variables := map[string]string{
        "PERSIST_IMAGE_NAME": fmt.Sprintf("$CSCS_REGISTRY_PATH/public/%v:latest", new_branch),
    }
    err = util.TriggerPipeline(h.glserver, &h.config, &h.db, pipeline_id, new_branch, variables)

    results_url := fmt.Sprintf("Build results at: %v/%v/%v?image=%v\n", h.config.BaseURL, h.config.URLPrefix, r.URL.Path, new_branch)
    w.Write([]byte(results_url))
}

func (h containerBuild) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    branch_name := r.URL.Query().Get("image")
    if branch_name == "" {
        pie(logger, condition_error{"Parameter image is empty"}, "Parameter image is empty", http.StatusBadRequest)
    }
    pipeline_id := h.config.Gitlab.ContainerBuilderServicePipelineId
    repo_id := h.db.GetRepositoryId(pipeline_id)
    mirror_project := fmt.Sprintf("%v/%v/%v", h.config.Gitlab.MirrorsPath, repo_id, pipeline_id)
    pipeline, _, err := h.glserver.Pipelines.GetLatestPipeline(mirror_project, &gitlab.GetLatestPipelineOptions{Ref: &branch_name})
    pie(logger, err, "Failed getting pipeline", http.StatusBadRequest)
    jobs, _, err := h.glserver.Jobs.ListPipelineJobs(mirror_project, pipeline.ID, &gitlab.ListJobsOptions{IncludeRetried: gitlab.Bool(false)})
    pie(logger, err, "Failed getting pipeline's jobs", http.StatusBadRequest)

    redirect_to := fmt.Sprintf("%v/%v/job/result/%v/%v/%v", h.config.BaseURL, h.config.URLPrefix, pipeline_id, pipeline.ProjectID, jobs[0].ID)
    http.Redirect(w, r, redirect_to, http.StatusFound)
}
