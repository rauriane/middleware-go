package handler

import (
    "fmt"
    "net/http"
    "os"
    "strings"

    "gopkg.in/yaml.v3"
    "github.com/xanzy/go-gitlab"
    gitconfig "github.com/go-git/go-git/v5/config"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


func GetTriggerPipelineHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(triggerPipelineHandler{glserver: glserver, db: db, config: config})
}

type triggerPipelineHandler struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

type triggerPipelinePostBody struct {
    Ref  string `yaml:"ref"`
    Pipeline  string `yaml:"pipeline"`
    Variables map[string]string `yaml:"variables"`
}

func (h triggerPipelineHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    repo_id := authorized_for_repo(r)
    if repo_id == -1 {
        w.Header().Set("WWW-Authenticate", `Basic realm="Trigger pipeline", charset="UTF-8"`)
        http.Error(w, "Need login to trigger pipeline", http.StatusUnauthorized)
        return
    }
    var trigger_descr triggerPipelinePostBody
    err := yaml.NewDecoder(r.Body).Decode(&trigger_descr)
    pie(logger, err, fmt.Sprintf("The provided data in body could not be parsed. Error=%v", err), http.StatusBadRequest)

    pipeline_id := h.db.GetPipelineId(trigger_descr.Pipeline, repo_id)
    if pipeline_id == -1 {
        err = pipeline_not_found_error{trigger_descr.Pipeline}
        pie(logger, err, err.Error(), http.StatusBadRequest)
    }
    reqLogFile, err := os.OpenFile(h.config.GetRequestLogPath(logging.GetReqCorrelationID(r)), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
    pie(logger, err, "Failed opening requests log file for writing", http.StatusInternalServerError)
    logger, reqId := logging.LogReqHandlingToFile(r, reqLogFile)
    logger.Debug().Msg("Started request logging")
    // continue in background - we have some work to do
    go func() {
        defer func() {
            logger.Info().Msg("Finished processing webhook event")
            reqLogFile.Close()
        }()

        target_path := fmt.Sprintf("/home/tmp/%v", repo_id)
        util.LockRepoDir(target_path, logger)
        defer util.UnlockRepoDir(target_path, logger)
        repo, err := util.GitCloneOrOpen(&h.db, &h.config, target_path, repo_id, true, logger, reqLogFile)
        if err != nil {
            logger.Error().Err(err).Msgf("Failed cloning repository error=%v", err)
            return
        }
        if err := util.GitFetch(&h.db, &h.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/*:refs/heads/*")}, repo_id, logger, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed fetching updates from remote. Error=%v", err)
            return
        }
        if err := util.EnsureMirrorRepo(&h.db, &h.config, h.glserver, logger, repo_id, pipeline_id); err != nil {
            logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
            return
        }
        refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/%v:refs/heads/%v", trigger_descr.Ref, trigger_descr.Ref))}
        if err := util.GitPush(&h.db, &h.config, repo, refspecs, repo_id, pipeline_id, logger, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
            return
        }
        if err := util.TriggerPipeline(h.glserver, &h.config, &h.db, pipeline_id, trigger_descr.Ref, trigger_descr.Variables); err != nil {
            logging.Errorf(err, "Failed triggering pipeline err=%v", err)
        }
    }()
    w.Write([]byte(strings.Replace(request_log_html, "REPLACE_URL", fmt.Sprintf("%v/%v/webhook_log?reqId=%v", h.config.BaseURL, h.config.URLPrefix, reqId), 1)))
}
