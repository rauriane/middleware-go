package util

import (
    "errors"
    "fmt"
    "os"

    "gopkg.in/yaml.v3"
    "github.com/go-sql-driver/mysql"

    "cscs.ch/cicd-ext-mw/logging"
)

type DBConfig struct {
    User string `yaml:"user"`
    Password string `yaml:"password"`
    Net string `yaml:"net"`
    Address string `yaml:"address"`
    DBName string `yaml:"dbname"`
}
type ListenAddressConfig struct {
    Address string `yaml:"address"`
    Port int `yaml:"port"`
}
type GitlabConfig struct {
    Url string `yaml:"url"`
    MirrorsPath string `yaml:"mirrors_path"`
    ContainerBuilderServicePipelineId int64 `yaml:"container_builder_service_project"`
    Token string `yaml:"token"`
}
type JFrogConfig struct {
    Username string `yaml:"username"`
    Token string `yaml:"token"`
    URL string `yaml:"url"`
    EMail string `yaml:"email"`
}
type SSHKeyPathConfig struct {
    Orig string `yaml:"orig"`
    Mirror string `yaml:"mirror"`
}
type TemplatesConfig struct {
    Dir string `yaml:"dir"`
    AlwaysInclude []string `yaml:"always_include"`
}
type SpackBuildcacheConfig struct {
    Cloud string `yaml:"cloud"`
    SignKeyPath string `yaml:"sign_key_path"`
}
type RunnerObserverConfig struct {
    Output string `yaml:"output"`
    Runners []int `yaml:"runners"`
}
type Config struct {
    DB DBConfig `yaml:"db"`
    Listen ListenAddressConfig `yaml:"listen"`
    MaxBodySize int64 `yaml:"max_body_size"`
    BaseURL string `yaml:"baseurl"`
    URLPrefix string `yaml:"urlprefix"`
    RequestsLogDir string `yaml:"requests_log_dir"`
    ErrorsDir string `yaml:"errors_dir"`
    Templates TemplatesConfig `yaml:"templates"`
    Gitlab GitlabConfig `yaml:"gitlab"`
    RegistryPath string `yaml:"registry_path"`
    JFrog JFrogConfig `yaml:"jfrog"`
    SSHKeyPath SSHKeyPathConfig `yaml:"ssh_key_path"`
    SpackBuildcache SpackBuildcacheConfig `yaml:"spack_buildcache"`
    RunnerObserver RunnerObserverConfig `yaml:"runner_observer"`
}


func ReadConfig(path string) Config {
    logger := logging.Get()
    data, err := os.ReadFile(path)
    if err != nil {
        logger.Error().Err(err).Msgf("Error opening config file `%v`", path)
        panic(err)
    }
    var config Config
    if err := yaml.Unmarshal(data, &config); err != nil {
        logger.Error().Err(err).Msg("Error unmarshalling config from yaml file")
        panic(err)
    }
    logger.Debug().Msgf("Read config: %+v", config)

    // sanity checks that config file has everything we want
    if config.MaxBodySize <= 0 {
        logger.Error().Msgf("Read MaxBodySize=%v from config file, which is not a valid value. Please specify a value > 0.", config.MaxBodySize)
        panic("Incorrect MaxBodySize config")
    }

    if config.BaseURL == "" {
        logger.Error().Msgf("Read BaseURL=%q from config file, which is not a valid value. Please specify a non-empty value.", config.BaseURL)
        panic("Incorrect BaseURL")
    }
    if config.URLPrefix == "" {
        logger.Error().Msgf("Read URLPrefix=%q from config file, which is not a valid value. Please specify a non-empty value.", config.URLPrefix)
        panic("Incorrect URLPrefix config")
    }

    if stat, err := os.Stat(config.Templates.Dir); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msgf("Read Templates.Dir=%q from config file, which does not point to an existing directory. Please specify a valid directory", config.Templates.Dir)
        panic("Incorrect Templates.Dir")
    }
    // Templates.AlwaysInclude should not be empty, and every file must exist
    if len(config.Templates.AlwaysInclude) == 0 {
        logger.Error().Msgf("Empty list Templates.AlwaysInclude. This is probably wrong")
        panic("Incorrect Templates.AlwaysInclude")
    }
    for _, template_path := range config.Templates.AlwaysInclude {
        if stat, err := os.Stat(fmt.Sprintf("%v/%v", config.Templates.Dir, template_path)); errors.Is(err, os.ErrNotExist) || stat.Mode().IsRegular() == false {
            logger.Error().Msgf("Could not find template %v", template_path)
            panic("Template not found")
        }
    }

    if config.RegistryPath == "" {
        logger.Error().Msgf("Read RegistryPath=%q from config file which is not a valid value. Please specify a non-empty value.", config.RegistryPath)
        panic("Incorrect RegistryPath config")
    }

    if config.JFrog.Username == "" || config.JFrog.Token == "" || config.JFrog.URL == "" || config.JFrog.EMail == "" {
        logger.Error().Msg("The config values for JFrog are invalid. Mandatory fields are Username, Token, URL, EMail, and they are not allowed to be empty.")
        panic("Incorrect JFrog config")
    }

    if config.Gitlab.Url == ""  || config.Gitlab.MirrorsPath == "" || config.Gitlab.Token == "" || config.Gitlab.ContainerBuilderServicePipelineId ==  0 {
        logger.Error().Msg("Invalid Gitlab config. Mandatory fields are Url, MirrorsPath, Token")
        panic("Incorrect Gitlab config ")
    }

    if config.SpackBuildcache.Cloud == "" || config.SpackBuildcache.SignKeyPath == "" {
        logger.Error().Msg("Invalid SpackBuildcache config. Mandatory fields are Cloud and SignKeyPath")
        panic("Incorrect SpackBuildcache config")
    }
    if stat, err := os.Stat(config.SSHKeyPath.Orig); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("SSHKeyPath.Orig does not point to a directory")
        panic("SSHKeyPath.Orig does not point to a directory")
    }
    if stat, err := os.Stat(config.SSHKeyPath.Mirror); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("SSHKeyPath.Mirror does not point to a directory")
        panic("SSHKeyPath.Mirror does not point to a directory")
    }

    if config.ErrorsDir == "" {
        logger.Error().Msg("Invalid ErrorsDir config. It is not allowed to be unset")
        panic("IncorrectErrorsDir config")
    }
    if stat, err := os.Stat(config.ErrorsDir); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("ErrorsDir does not point to an existing directory")
        panic("ErrorsDir does not point to an existing director")
    }

    if config.RunnerObserver.Output == "" || len(config.RunnerObserver.Runners) == 0 {
        logger.Error().Msg("Invalid RunnerObserver config. Mandatory fields are Output and Runners")
        panic("Incorrect RunnerObserver config")
    }

    if config.RequestsLogDir == "" {
        logger.Error().Msg("Invalid RequestsLogDir. It cannot be an empty string")
        panic("Incorrect RequestsLogDir")
    }
    if stat, err := os.Stat(config.RequestsLogDir); errors.Is(err, os.ErrNotExist) || stat.IsDir() == false {
        logger.Error().Msg("RequestsLogDir does not point to an existing directory")
        panic("RequestsLogDir ddoes not point to a directory")
    }

    return config
}

func (cfg *Config) GetRegistryPathForRepo(repository_id int64) string {
    return fmt.Sprintf("%v/%v", cfg.RegistryPath, repository_id)
}

func (cfg *Config) GetSSHKeyPath(repo_or_pipeline_id int64, mirror bool, public bool) string {
    var path string
    if mirror {
        path = fmt.Sprintf("%v/key_%v", cfg.SSHKeyPath.Mirror, repo_or_pipeline_id)
    } else {
        path = fmt.Sprintf("%v/key_%v", cfg.SSHKeyPath.Orig, repo_or_pipeline_id)
    }
    if public {
        path += ".pub"
    }
    return path
}

func (cfg *Config) GetSshKey(repo_or_pipeline_id int64, mirror bool, pubkey bool) ([]byte, error) {
    return os.ReadFile(cfg.GetSSHKeyPath(repo_or_pipeline_id, mirror, pubkey))
}

func (cfg *Config) GetSpackSignKey() ([]byte, error) {
    return os.ReadFile(cfg.SpackBuildcache.SignKeyPath)
}

func (cfg *Config) GetDBPath() string {
    mysqlConfig := mysql.NewConfig()
    mysqlConfig.User = cfg.DB.User
    mysqlConfig.Passwd = cfg.DB.Password
    mysqlConfig.Addr = cfg.DB.Address
    mysqlConfig.DBName = cfg.DB.DBName
    mysqlConfig.Net = cfg.DB.Net
    return mysqlConfig.FormatDSN()
}

func (cfg *Config) GetRequestLogPath(reqId string) string {
    return fmt.Sprintf("%v/req-%v.log", cfg.RequestsLogDir, reqId)
}
